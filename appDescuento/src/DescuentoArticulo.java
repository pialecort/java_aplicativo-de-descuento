import java.util.Scanner;

public class DescuentoArticulo {
    public static void main(String[] args) {

        //invoca clase scanner
        Scanner s = new Scanner(System.in);

        //Declaración de variables
        String nombre, clave;
        double precio, descuento=0;

        //Ingresar datos por consola
        System.out.println("Ingrese el nombre del artículo");
        nombre = s.next();
        System.out.println("ingrese clave (01 ó 02)");
        clave = s.next();
        System.out.println("Ingrese el precio");
        precio = s.nextDouble();

        //Lógica de aplicación de descuento
        if(clave.equals("01")){
            descuento = precio * 0.1;
        }
        if(clave.equals("02")){
            descuento = precio * 0.2;
        }

        //Salida de datos
        System.out.println("nombre: " + nombre);
        System.out.println("clave: " + clave);
        System.out.println("Precio: " + precio);
        System.out.println("Descuento: " + descuento);
        System.out.println("Precio con descuento: " + (precio -descuento));

    } //fin método main
}